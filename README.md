# Distro_Packages

## Description
This repository holds lists of packages for different linux distributions 
that are required for desktop environment.

For each linux distribution there is a specific branch in repository, which
you can checkout and use.

## Currently supported distributions
- archlinux
- ubuntu
